FROM node:17-alpine3.14
WORKDIR: /usr/share/app
COPY ./app/package*.json ./
RUN npm install
COPY: ./ .
CMD ["node", "server.js"]

